<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class IntervalsSeeder
 */
class PeriodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('periods')->insert([
            'name' => 'Последний час',
            'alias' => 'lastHour',
            'expression' => '1 HOUR',
        ]);
        DB::table('periods')->insert([
            'name' => 'Последние сутки',
            'alias' => 'lastDay',
            'expression' => '1 DAY',
        ]);
    }
}
