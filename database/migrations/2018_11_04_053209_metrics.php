<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Metrics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metrics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 64)->comment('Название метрики');
            $table->unsignedInteger('resource_id')
                ->comment('Источник данных')
                ->nullable();
            $table->string('resource_value', 256)
                ->comment('Путь до значения в данных')
                ->nullable();

            $table->foreign('resource_id', 'fk-metrics-resource')
                ->references('id')
                ->on('resources')
                ->onDelete('SET NULL');
        });

        Schema::create('metrics_values', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('metric_id')->comment('Метрика');
            $table->decimal('value')->comment('Значение метрики');
            $table->timestamp('gathered_at')
                ->comment('Время получения метрики')
                ->useCurrent()
                ->index();

            $table->foreign('metric_id', 'fk-metrics_values-metric')
                ->references('id')
                ->on('metrics')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metrics_values');
        Schema::dropIfExists('metrics');
    }
}
