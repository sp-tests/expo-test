<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware(['auth:accessToken'])->group(function () {
    Route::apiResource('metrics', 'Api\\MetricsController')
        ->only(['index', 'show']);
    Route::apiResource('dashboards/elements', 'Api\\DashboardsElementsController');
    Route::apiResource('dashboards', 'Api\\DashboardsController');
    Route::get('/metrics/{id}/values', 'Api\\MetricsController@values');
    Route::post('/metrics/{id}/values', 'Api\\MetricsController@storeValue');
});