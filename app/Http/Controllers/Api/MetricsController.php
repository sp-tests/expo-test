<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Resources\Metric as MetricsResource;
use App\Http\Controllers\Controller;
use App\Http\Resources\MetricValue;
use App\Models\Period;
use App\Services\MetricsService;
use App\Models\Metric;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class MetricsController
 * @package App\Controllers\Api
 */
class MetricsController extends Controller
{
    /**
     * @var MetricsService
     */
    protected $metricService;

    /**
     * MetricsController constructor.
     * @param MetricsService $metricsService
     */
    public function __construct(MetricsService $metricsService)
    {
        $this->metricService = $metricsService;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return MetricsResource::collection(
            Metric::with('resource')->get()
        );
    }

    /**
     * @param Request $request
     * @param $id
     * @return MetricsResource
     */
    public function show(Request $request, $id)
    {
        return new MetricsResource(
            Metric::with('resource')->find($id)
        );
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function values(Request $request, $id)
    {
        $request->validate([
            'period' => 'sometimes|required|exists:periods,alias',
            'from' => 'sometimes|required_without:period|date_format:Y-m-d',
            'to' => 'sometimes|required_without:period|date_format:Y-m-d',
        ]);

        $period = $request->input('period');
        $from = $request->input('from');
        $to = $request->input('to');

        if ($period) {
            $from = $to = null;
            $period = Period::where('alias', $period)->first();
        }

        $query = \App\Models\MetricValue::query()
            ->select(['gathered_at', 'value'])
            ->where('metric_id', $id)
            ->when($period, function (Builder $query, Period $period) {
                $query->where(
                    'gathered_at',
                    '>=',
                    DB::raw("(NOW() - INTERVAL {$period->expression})")
                );
            })
            ->when($from, function (Builder $query, $from) {
                $query->where('gathered_at', '>=', $from);
            })
            ->when($to, function (Builder $query, $to) {
                $query->where('gathered_at', '<=', $to);
            });

        return MetricValue::collection($query->get());
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function storeValue(Request $request, $id)
    {
        $request->validate([
            'value' => 'required|numeric',
        ]);

        $metric = Metric::findOrFail($id);
        $value = $request->get('value');

        if (!$this->metricService->storeValue($metric, $value)) {
            return response()->json(['message' => "Can\'t save value for metric"], 400);
        }

        return response()->json(null, 201);
    }
}