<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class DashboardElementResource
 * @package App\Http\Resources
 */
class DashboardElementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'dashboard_id' => $this->dashboard_id,
            'metric_id' => $this->metric_id,
            'order' => $this->order,
            'view_type' => $this->view_type,
            'chart_type' => $this->chart_type,
            'period_id' => $this->period_id,
        ];
    }
}
