<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class MetricValue
 * @package App\Http\Resources
 */
class MetricValue extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'date' => $this->gathered_at,
            'value' => $this->value
        ];
    }
}
