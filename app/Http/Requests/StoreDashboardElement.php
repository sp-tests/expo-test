<?php

namespace App\Http\Requests;

use App\Models\DashboardElement;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class StoreDashboardElement
 * @package App\Http\Requests
 */
class StoreDashboardElement extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $requiredForChart = "required_if:view_type," . DashboardElement::VIEW_TYPE_CHART;

        return [
            'dashboard_id' => 'required|exists:dashboards,id',
            'metric_id' => 'required|exists:metrics,id',
            'period_id' => $requiredForChart . '|exists:periods,id',
            'order' => 'required|integer',
            'view_type' => [
                'required',
                Rule::in([
                    DashboardElement::VIEW_TYPE_CHART,
                    DashboardElement::VIEW_TYPE_NUMBER,
                ])
            ],
            'chart_type' => [
                $requiredForChart,
                Rule::in([
                    DashboardElement::CHART_TYPE_DIAGRAM,
                    DashboardElement::CHART_TYPE_LINEAR,
                ])
            ],
        ];
    }
}
