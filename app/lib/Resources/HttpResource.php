<?php

declare(strict_types=1);

namespace App\lib\Resources;

use Http\Client\HttpClient;
use Http\Message\MessageFactory;
use Psr\Http\Message\RequestInterface;

/**
 * Class HttpResource
 * @package App\lib\Resources
 */
class HttpResource implements LocatedResourceInterface
{
    use LocatedResource;

    /**
     * @var HttpClient
     */
    protected $client;

    /**
     * @var MessageFactory
     */
    protected $messageFactory;

    /**
     * @var string
     */
    public $method = 'get';

    /**
     * HttpResource constructor.
     * @param HttpClient $client
     * @param MessageFactory $messageFactory
     */
    public function __construct(HttpClient $client, MessageFactory $messageFactory)
    {
        $this->client = $client;
        $this->messageFactory = $messageFactory;
    }

    /**
     * @return string
     * @throws \Http\Client\Exception
     */
    public function getData(): string
    {
        $this->ensurePathIsDefined();
        $request = $this->buildRequest();
        $response = $this->client->sendRequest($request);

        return $response->getBody()->getContents();
    }

    /**
     * @return \Psr\Http\Message\RequestInterface
     */
    protected function buildRequest(): RequestInterface
    {
        return $this->messageFactory->createRequest(
            $this->method,
            $this->path
        );
    }
}