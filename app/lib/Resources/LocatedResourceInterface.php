<?php

declare(strict_types=1);

namespace App\lib\Resources;

/**
 * Интерфейс для ресурсов, которые имеют определенный путь(http, file, etc...)
 * Т.к. вполне могут быть ресурсы для которых не нужно указывать путь, напр-р: php://stdin, StringResource и т.д.
 *
 * Interface LocatedResourceInterface
 * @package App\lib\Resources
 */
interface LocatedResourceInterface extends ResourceInterface
{
    /**
     * @param string $path
     * @return $this
     */
    public function setPath(string $path);

    /**
     * @return string
     */
    public function getPath(): string;
}