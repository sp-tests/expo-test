<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Resource
 * @package App\Models
 *
 * @property int $id
 * @property string $type
 * @property string $content_type
 * @property string $path
 */
class Resource extends Model
{
    const TYPE_HTTP = 'http';
    const CONTENT_TYPE_RAW = 'raw';
    const CONTENT_TYPE_JSON = 'json';
    const CONTENT_TYPE_XML = 'xml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function metric()
    {
        return $this->hasOne('App\Models\Metric');
    }
}
