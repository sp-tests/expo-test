<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Справочник периодов
 *
 * Class Interval
 * @package App\Models
 *
 * @property int $id
 * @property string $alias
 * @property string $name
 * @property string $expression
 */
class Period extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;
}
