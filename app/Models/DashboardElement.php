<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DashboardElement
 * @package App\Models
 *
 * @property int $id
 * @property int $order
 * @property int $dashboard_id
 * @property int $metric_id
 * @property string $view_type
 * @property string $chart_type
 * @property string $period
 */
class DashboardElement extends Model
{
    const VIEW_TYPE_NUMBER = 'number';
    const VIEW_TYPE_CHART = 'chart';

    const CHART_TYPE_LINEAR = 'linear';
    const CHART_TYPE_DIAGRAM = 'diagram';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['dashboard_id', 'metric_id', 'view_type', 'chart_type', 'period_id', 'order'];
}
