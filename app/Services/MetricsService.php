<?php

declare(strict_types=1);

namespace App\Services;

use App\Events\NewMetricValueEvent;
use App\Models\Metric;
use App\Models\MetricValue;
use App\lib\Parsers\ParserFactory;
use App\lib\Resources\ResourceFactory;

/**
 * Class MetricsService
 * @package App\Services
 */
class MetricsService
{
    /**
     * @var ResourceFactory
     */
    protected $resourceFactory;

    /**
     * @var ParserFactory
     */
    protected $parserFactory;

    /**
     * Create a new command instance.
     *
     * @param ResourceFactory $resourceFactory
     * @param ParserFactory $parserFactory
     */
    public function __construct(ResourceFactory $resourceFactory, ParserFactory $parserFactory)
    {
        $this->resourceFactory = $resourceFactory;
        $this->parserFactory = $parserFactory;
    }

    /**
     * Получение текущего значения метрики из ресурса
     *
     * @param Metric $metric
     * @return mixed
     */
    public function getResourceValue(Metric $metric)
    {
        $resourceModel = $metric->resource;

        if (!$resourceModel) {
            throw new \InvalidArgumentException('Metric resource is not defined');
        }

        $parser = $this->parserFactory->createParser($resourceModel->content_type);
        $resourse = $resourceModel->path
            ? $this->resourceFactory->createLocatedResource(
                $resourceModel->type,
                $resourceModel->path
            )
            : $this->resourceFactory->createResource($resourceModel->type);

        return $parser->getValue(
            $resourse->getData(),
            $metric->resource_value
        );
    }

    /**
     * Сохранение полученного значения метрики
     *
     * @param Metric $metric
     * @param $value
     * @return bool
     */
    public function storeValue(Metric $metric, $value): bool
    {
        $metricValue = new MetricValue();
        $metricValue->value = $value;

        $success = (bool)$metric->values()->save($metricValue);

        if ($success) {
            broadcast(new NewMetricValueEvent($metricValue));
        }

        return $success;
    }
}